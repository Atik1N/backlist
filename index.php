﻿<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

$isHttps = !empty($_SERVER['HTTPS']) && 'off' !== strtolower($_SERVER['HTTPS']);
define ("HTTP_HOST", $isHttps ? "https://". $_SERVER['HTTP_HOST'] . "/" : "http://". $_SERVER['HTTP_HOST'] . "/");

require_once 'config.php';
require_once 'application/bootstrap.php';
