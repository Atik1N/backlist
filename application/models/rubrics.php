<?php

class ModelRubrics extends Model
{
	public function show($rubric_id = false)
	{
        $list_rubrics = array();
        if($rubric_id === false)
            $result = $this->connect->query("SELECT * FROM `rubrics` GROUP BY `rubric_id` DESC") or die($this->connect->error);
        else
            $result = $this->connect->query("SELECT * FROM `rubrics` WHERE `rubric_id` = '" . (int)$rubric_id . "'") or die($this->connect->error);

        while ($rubric = $result->fetch_array()){
            $list_rubrics[] = array(
                'rubric_id' => $rubric['rubric_id'],
                'parent_id' => $rubric['parent_id'],
                'name'         => $rubric['name']
            );
        }
        return $list_rubrics;
	}

    public function update($data)
    {
        $this->connect->query("UPDATE `rubrics` SET `name` = '" . $this->escape($data['name']) . "', `parent_id` = '" . (int)$data['parent_id'] . "' WHERE `rubric_id` = '" . (int)$data['rubric_id'] . "'") or die($this->connect->error);
    }

    public function insert($data)
    {
        $this->connect->query("INSERT INTO `rubrics` SET `name` = '" . $this->escape($data['name']) . "', `parent_id` = '" . (int)$data['parent_id'] . "'") or die($this->connect->error);
    }

    public function delete($rubric_id)
    {
        $this->connect->query("DELETE FROM `rubrics` WHERE `rubric_id` = '" . (int)$rubric_id . "'") or die($this->connect->error);
    }

    public function getRubricsByParentId($parent_id)
    {
        $result = $this->connect->query("SELECT *, (SELECT COUNT(parent_id) FROM rubrics WHERE parent_id = r.rubric_id) AS children FROM rubrics r WHERE r.parent_id = '" . (int)$parent_id . "' ORDER BY r.name") or die($this->connect->error);
        return $result;
    }

    public function check($rubric_id){

        $result = 0;
        $query = $this->connect->query("SELECT COUNT(*) FROM `book_to_rubric` WHERE `rubric_id` = '" . (int)$rubric_id . "'") or die($this->connect->error);
        if($query->num_rows)
            $result = $query->fetch_array()[0];
        return $result;
    }

}
