<?php

class ModelPublishers extends Model
{
	
	public function show($publisher_id = false)
	{

        $list_publishers = array();
        if($publisher_id === false)
            $result = $this->connect->query("SELECT * FROM `publishers` GROUP BY `publisher_id` DESC") or die($this->connect->error);
        else
            $result = $this->connect->query("SELECT * FROM `publishers` WHERE `publisher_id` = '" . (int)$publisher_id . "'") or die($this->connect->error);

        while ($publisher = $result->fetch_array()){
            $list_publishers[] = array(
                'publisher_id' => $publisher['publisher_id'],
                'name'         => $publisher['name'],
                'address'      => $publisher['address'],
                'telephone'    => $publisher['telephone']
            );
        }
        return $list_publishers;
	}

    public function update($data)
    {
        $this->connect->query("UPDATE `publishers` SET `name` = '" . $this->escape($data['name']) . "', `address` = '" . $this->escape($data['address']) . "', `telephone` = '" . $this->escape($data['telephone']) . "' WHERE `publisher_id` = '" . (int)$data['publisher_id'] . "'") or die($this->connect->error);
    }

    public function insert($data)
    {
        $this->connect->query("INSERT INTO `publishers` SET `name` = '" . $this->escape($data['name']) . "', `address` = '" . $this->escape($data['address']) . "', `telephone` = '" . $this->escape($data['telephone']) . "'") or die($this->connect->error);
    }

    public function delete($publisher_id)
    {
        $this->connect->query("DELETE FROM `publishers` WHERE `publisher_id` = '" . (int)$publisher_id . "'") or die($this->connect->error);
    }

    public function check($publisher_id){

        $result = 0;
        $query = $this->connect->query("SELECT COUNT(*) FROM `book_to_publisher` WHERE `publisher_id` = '" . (int)$publisher_id . "'") or die($this->connect->error);
        if($query->num_rows)
            $result = $query->fetch_array()[0];
        return $result;
    }

}
