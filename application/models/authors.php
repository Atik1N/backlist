<?php

class ModelAuthors extends Model
{
	
	public function show($author_id = false)
	{

        $list_authors = array();
        if($author_id === false)
            $result = $this->connect->query("SELECT * FROM `authors` GROUP BY `author_id` DESC") or die($this->connect->error);
        else
            $result = $this->connect->query("SELECT * FROM `authors` WHERE `author_id` = '" . (int)$author_id . "'") or die($this->connect->error);

        while ($author = $result->fetch_array()){
            $list_authors[] = array(
                'author_id' => $author['author_id'],
                'name'         => $author['name']
            );
        }
        return $list_authors;
	}

    public function update($data)
    {
        $this->connect->query("UPDATE `authors` SET `name` = '" . $this->escape($data['name']) . "' WHERE `author_id` = '" . (int)$data['author_id'] . "'") or die($this->connect->error);
    }

    public function insert($data)
    {
        $this->connect->query("INSERT INTO `authors` SET `name` = '" . $this->escape($data['name']) . "'") or die($this->connect->error);
    }

    public function delete($author_id)
    {
        $this->connect->query("DELETE FROM `authors` WHERE `author_id` = '" . (int)$author_id . "'") or die($this->connect->error);
    }

    public function check($author_id){

        $result = 0;
        $query = $this->connect->query("SELECT COUNT(*) FROM `book_to_author` WHERE `author_id` = '" . (int)$author_id . "'") or die($this->connect->error);
        if($query->num_rows)
            $result = $query->fetch_array()[0];
        return $result;
    }

}
