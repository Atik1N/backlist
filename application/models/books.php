<?php

class ModelBooks extends Model
{

    public function show($book_id = false, $detail = false)
    {
        $list_books = array();
        if($book_id === false)
            $result = $this->connect->query("SELECT * FROM `books` b LEFT JOIN `book_to_publisher` bp ON (bp.book_id = b.book_id) LEFT JOIN `book_to_rubric` br ON (br.book_id = b.book_id) GROUP BY b.`book_id` DESC") or die($this->connect->error);
        else
            $result = $this->connect->query("SELECT * FROM `books` b LEFT JOIN `book_to_publisher` bp ON (bp.book_id = b.book_id) LEFT JOIN `book_to_rubric` br ON (br.book_id = b.book_id) WHERE b.`book_id` = '" . (int)$book_id . "'") or die($this->connect->error);

        while ($book = $result->fetch_array()){
            $list_books[] = array(
                'book_id'               => $book['book_id'],
                'name'                  => $book['name'],
                'date_publication'      => date("d.m.Y", strtotime($book['date_publication'])),
                'authors_id'            => $detail ? $this->getAuthors($book['book_id']) : $this->getAuthorsID($book['book_id']),
                'publisher_id'          => $detail ? $this->getPublishers($book['book_id']) : $book['publisher_id'],
                'rubric_id'             => $detail ? $this->getRubrics($book['book_id']) : $book['rubric_id'],
                'images'                => $this->getImages($book['book_id'])
            );
        }
        return $list_books;
    }

    public function update($data)
    {
        $date_publication = date("Y-m-d", strtotime($data['date_publication']));

        $this->connect->query("UPDATE `books` SET `name` = '" . $this->escape($data['name']) . "', `date_publication` = '" . $date_publication . "' WHERE `book_id` = '" . (int)$data['book_id'] . "'") or die($this->connect->error);

        $this->connect->query("DELETE FROM `book_to_author` WHERE `book_id` = '" . (int)$data['book_id'] . "'");
        foreach ($data['authors_id'] as $author_id) {
            $this->connect->query("INSERT INTO `book_to_author` SET `author_id` = '" . (int)$author_id . "', `book_id` = '" . (int)$data['book_id'] . "'") or die($this->connect->error);
        }

        $this->connect->query("UPDATE `book_to_publisher` SET `publisher_id` = '" . (int)$data['publisher_id'] . "' WHERE `book_id` = '" . (int)$data['book_id'] . "'") or die($this->connect->error);

        $this->connect->query("UPDATE `book_to_rubric` SET `rubric_id` = '" . (int)$data['rubric_id'] . "' WHERE `book_id` = '" . (int)$data['book_id'] . "'") or die($this->connect->error);

        $this->connect->query("DELETE FROM `book_images` WHERE `book_id` = '" . (int)$data['book_id'] . "'");
        if(isset($data['images'])) {
            foreach ($data['images'] as $path) {
                $this->connect->query("INSERT INTO `book_images` SET `path` = '" . $this->escape($path) . "', `book_id` = '" . (int)$data['book_id'] . "'") or die($this->connect->error);
            }
        }
    }

    public function insert($data)
    {
        $date_publication = date("Y-m-d", strtotime($data['date_publication']));

        $this->connect->query("INSERT INTO `books` SET `name` = '" . $this->escape($data['name']) . "', `date_publication` = '" . $date_publication . "'") or die($this->connect->error);
        $book_id = $this->connect->insert_id;

        foreach ($data['authors_id'] as $author_id) {
            $this->connect->query("INSERT INTO `book_to_author` SET `author_id` = '" . (int)$author_id . "', `book_id` = '" . $book_id . "'") or die($this->connect->error);
        }

        $this->connect->query("INSERT INTO `book_to_publisher` SET `publisher_id` = '" . (int)$data['publisher_id'] . "', `book_id` = '" . $book_id . "'") or die($this->connect->error);

        $this->connect->query("INSERT INTO `book_to_rubric` SET `rubric_id` = '" . (int)$data['rubric_id'] . "', `book_id` = '" . $book_id . "'") or die($this->connect->error);

        if(isset($data['images'])) {
            foreach ($data['images'] as $path) {
                $this->connect->query("INSERT INTO `book_images` SET `path` = '" . $this->escape($path) . "', `book_id` = '" . $book_id . "'") or die($this->connect->error);
            }
        }

    }

    public function delete($book_id)
    {
        $this->connect->query("DELETE FROM `books` WHERE `book_id` = '" . (int)$book_id . "'") or die($this->connect->error);
        $this->connect->query("DELETE FROM `book_to_rubric` WHERE `book_id` = '" . (int)$book_id . "'") or die($this->connect->error);
        $this->connect->query("DELETE FROM `book_to_publisher` WHERE `book_id` = '" . (int)$book_id . "'") or die($this->connect->error);
        $this->connect->query("DELETE FROM `book_to_author` WHERE `book_id` = '" . (int)$book_id . "'") or die($this->connect->error);
        $this->connect->query("DELETE FROM `book_images` WHERE `book_id` = '" . (int)$book_id . "'") or die($this->connect->error);

    }

    public function getAuthors($book_id) {

        $result = array();
        $query = $this->connect->query("SELECT a.author_id, a.name FROM `book_to_author` b  LEFT JOIN `authors` a ON (a.author_id = b.author_id) WHERE `book_id` = '" . (int)$book_id . "'") or die($this->connect->error);
        while ($item = $query->fetch_array()){

            $result[] = array(
                'author_id' => $item['author_id'],
                'name'      => $item['name']
            );
        }
        return $result;
    }

    public function getAuthorsID($book_id, $short = false) {

        $result = array();
        $query = $this->connect->query("SELECT `author_id` FROM `book_to_author` WHERE `book_id` = '" . (int)$book_id . "'") or die($this->connect->error);
        while ($item = $query->fetch_array()){
            $result[] = $item['author_id'];
        }
        return $result;
    }

    public function getPublishers($book_id) {

        $result = array();
        $query = $this->connect->query("SELECT p.publisher_id, p.name, p.address, p.telephone FROM `book_to_publisher` b  LEFT JOIN `publishers` p ON (p.publisher_id = b.publisher_id) WHERE `book_id` = '" . (int)$book_id . "'") or die($this->connect->error);
        while ($item = $query->fetch_array()){
            $result[] = array(
                'publisher_id'  => $item['publisher_id'],
                'name'          => $item['name'],
                'address'       => $item['address'],
                'telephone'     => $item['telephone']
            );
        }
        return $result;
    }

    public function getRubrics($book_id) {

        $result = array();
        $query = $this->connect->query("SELECT r.rubric_id, r.name FROM `book_to_rubric` b  LEFT JOIN `rubrics` r ON (r.rubric_id = b.rubric_id) WHERE `book_id` = '" . (int)$book_id . "'") or die($this->connect->error);
        while ($item = $query->fetch_array()){
            $result[] = array(
                'rubric_id' => $item['rubric_id'],
                'name'      => $item['name']
            );
        }
        return $result;
    }

    public function getImages($book_id) {

        $result = array();
        $query = $this->connect->query("SELECT `path` FROM `book_images`  WHERE `book_id` = '" . (int)$book_id . "'") or die($this->connect->error);
        while ($item = $query->fetch_array()){
            $result[] = $item['path'];
        }
        return $result;
    }

}
