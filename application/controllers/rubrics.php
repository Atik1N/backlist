<?php

class ControllerRubrics extends Controller
{

    private $error = array();

    function __construct()
    {
        $this->view = new View();
        $this->model_rubrics = new ModelRubrics();
    }

    function index()
    {
        $data['text_title'] = 'Список рубрик';
        $data['rubrics'] = $this->getRubrics(0);
        $data['errors'] = $this->error;
        $this->view->load('rubrics.php', 'template.php', $data);
    }

    function view($rubric_id)
    {
        $data['rubric'] = $this->model_rubrics->show($rubric_id);
        $data['text_title'] = 'Просмотр информации о рубрике';
        $data['crud'] = 'view';
        if($data['rubric']) {
            $data['form_link'] = HTTP_HOST . "rubrics/" . $data['crud'] . "/" . $rubric_id;
            $data['rubrics_list'] = $this->model_rubrics->show();
            $data['rubric'] = $this->model_rubrics->row($data['rubric']);
            $this->view->load('rubric_form.php', 'template.php', $data);
        } else{
            $this->view->__404();
        }

    }

    function edit($route_rubric_id)
    {
        if (($_SERVER['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_rubrics->update($_POST);
            $this->redirect('rubrics');
        }
        if(isset($_POST['rubric_id'])){

            $rubric_id = $_POST['rubric_id'];
        } else if ($route_rubric_id) {

            $rubric_id = $route_rubric_id;
        } else {
            $rubric_id = 0;
        }
        $data['rubric'] = $this->model_rubrics->show($rubric_id);
        $data['text_title'] = 'Редактирование информации о рубрике';
        $data['rubrics_list'] = $this->model_rubrics->show();
        $data['crud'] = 'edit';
        if($data['rubric']) {
            $data['form_link'] = HTTP_HOST . "rubrics/" . $data['crud'] . "/" . $rubric_id;
            $data['rubric'] = $this->model_rubrics->row($data['rubric']);
            $this->view->load('rubric_form.php', 'template.php', $data);
        } else {
            $this->view->__404();
        }
    }

    function create()
    {
        if (($_SERVER['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_rubrics->insert($_POST);
            $this->redirect('rubrics');
        }
        $data['crud'] = 'create';
        $data['form_link'] = HTTP_HOST . "rubrics/" . $data['crud'];
        $data['text_title'] = 'Добавить рубрику';
        $data['rubrics_list'] = $this->model_rubrics->show();
        $data['rubric'] = $_POST;
        $data['errors'] = $this->error;
        $this->view->load('rubric_form.php', 'template.php', $data);
    }

    function delete($rubric_id)
    {
        if((int)$rubric_id !== 0) {

            if($this->validateDelete($rubric_id)) {
                $this->model_rubrics->delete($rubric_id);
                $this->redirect('rubrics');
            } else {
                $this->index();
            }
        } else {
            $this->view->__404();
        }
    }

    private function getRubrics($parent_id, $level = 0) {
        $output = array();

        $results = $this->model_rubrics->getRubricsByParentId($parent_id);
        $level ++;
        while ($result = $results->fetch_array()) {

            $name = $result['name'];

            $output[$result['rubric_id']] = array(
                'rubric_id' => $result['rubric_id'],
                'name'      => $name,
                'level'     => $level - 1
            );

            $output += $this->getRubrics($result['rubric_id'], $level);
        }

        return $output;
    }

    private function validate()
    {
        if ((mb_strlen($_POST['name']) < 3) || (mb_strlen($_POST['name']) > 255)) {
            $this->error['name'] = 'Введите имя';
        }
        return !$this->error;
    }

    private function validateDelete($rubric_id)
    {
        $results = $this->model_rubrics->getRubricsByParentId($rubric_id);

        if($results->num_rows) {
            $this->error['system_notification'][] = 'У данной рубрики есть потомки. Для начала удалите их';
        }
        $result = $this->model_rubrics->check($rubric_id);
        if((int)$result){
            $this->error['system_notification'][] = "Рубрика указана у нескольких книг: " . $result;
        }
        return !$this->error;
    }
}