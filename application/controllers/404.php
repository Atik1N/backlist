<?php

class Controller404 extends Controller
{
	
	function index()
	{
        $data['text_title'] = 'Страница не найдена';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
		$this->view->load('404.php', 'template.php', $data);
	}

}
