<?php

class ControllerAuthors extends Controller
{

    private $error = array();

    function __construct()
    {
        $this->model_authors = new ModelAuthors();
        $this->model_authors_books = new ModelBooks();
        $this->view = new View();
    }

    function index()
    {
        $data['text_title'] = 'Список авторов';
        $data['authors'] = $this->model_authors->show();
        $data['errors'] = $this->error;
        $this->view->load('authors.php', 'template.php', $data);
    }

    function view($author_id)
    {
        $data['author'] = $this->model_authors->show($author_id);
        $data['text_title'] = 'Просмотр информации об авторе';
        $data['crud'] = 'view';
        if($data['author']) {
            $data['form_link'] = HTTP_HOST . "authors/" . $data['crud'] . "/" . $author_id;
            $data['author'] = $this->model_authors->row($data['author']);
            $this->view->load('author_form.php', 'template.php', $data);
        } else{
            $this->view->__404();
        }
    }

    function edit($route_author_id)
    {
        if (($_SERVER['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_authors->update($_POST);
            $this->redirect('authors');
        }
        if(isset($_POST['author_id'])){

            $author_id = $_POST['author_id'];
        } else if ($route_author_id) {

            $author_id = $route_author_id;
        } else {
            $author_id = 0;
        }
        $data['author'] = $this->model_authors->show($author_id);
        $data['text_title'] = 'Редактирование информации об авторе';
        $data['crud'] = 'edit';
        if($data['author']) {
            $data['form_link'] = HTTP_HOST . "authors/" . $data['crud'] . "/" . $author_id;
            $data['author'] = $this->model_authors->row($data['author']);
            $this->view->load('author_form.php', 'template.php', $data);
        } else{
            $this->view->__404();
        }
    }

    function create()
    {
        if (($_SERVER['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_authors->insert($_POST);
            $this->redirect('authors');
        }
        $data['crud'] = 'create';
        $data['form_link'] = HTTP_HOST . "authors/" . $data['crud'];
        $data['text_title'] = 'Добавить автора';
        $data['author'] = $_POST;
        $data['errors'] = $this->error;
        $this->view->load('author_form.php', 'template.php', $data);
    }

    function delete($author_id)
    {
        if((int)$author_id !== 0) {
            if($this->validateDelete($author_id)) {
                $this->model_authors->delete($author_id);
                $this->redirect('authors');
            } else {
                $this->index();
            }
        } else {
            $this->view->__404();
        }
    }

    function validate()
    {
        if ((mb_strlen($_POST['name']) < 3) || (mb_strlen($_POST['name']) > 255)) {
            $this->error['name'] = 'Введите имя';
        }
        return !$this->error;
    }

    private function validateDelete($author_id)
    {
        $result = $this->model_authors->check($author_id);
        if((int)$result){
            $this->error['system_notification'][] = "Удаление не возможно. Автор указан в нескольких книгах: " . $result;
        }
        return !$this->error;
    }
}