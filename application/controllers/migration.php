<?php

class ControllerMigration extends Controller
{
    private $allFiles = array();
    public function __construct()
    {
        $this->view = new View();
        $this->model = new Model();
    }

	function index()
	{
        $data['text_title'] = 'Список миграций базы данных';
        $files = $this->getMigrationFiles();
        $data['relevance_status'] = empty($files);
        foreach ($this->allFiles as $file){
            $status = in_array($file, $files);
            $data['migrations'][] = array(
                'name' => basename($file),
                'status' => !(int)$status
            );
        }
		$this->view->load('migration.php', 'template.php', $data);
	}

    function start()
    {
        $files = $this->getMigrationFiles();

        if (!empty($files)) {

            foreach ($files as $file) {

                $this->migrate($file);
            }
        }
        $this->redirect('migration');
    }

    function getMigrationFiles() {
        $sqlFolder = str_replace('\\', '/', realpath('application/migrations') . '/');
        $this->allFiles = glob($sqlFolder . '*.sql');

        $query = sprintf('show tables from `%s` like "%s"', DB_NAME, DB_TABLE_VERSIONS);
        $data = $this->model->query($query);
        $firstMigration = !$data->num_rows;

        if ($firstMigration) {
            return $this->allFiles;
        }

        $versionsFiles = array();
        $query = sprintf('select `name` from `%s`', DB_TABLE_VERSIONS);
        $data = $this->model->query($query)->fetch_all(MYSQLI_ASSOC);

        foreach ($data as $row) {
            array_push($versionsFiles, $sqlFolder . $row['name']);
        }
        return array_diff($this->allFiles, $versionsFiles);
    }

    function migrate($file) {
        $command = sprintf('mysql -u%s -p%s -h %s -D %s --default-character-set=utf8 < %s', DB_USER,DB_PASS, DB_HOST, DB_NAME, $file);
        shell_exec($command);

        $baseName = basename($file);

        $query = sprintf('insert into `%s` (`name`) values("%s")', DB_TABLE_VERSIONS, $baseName);

        $this->model->query($query);
    }

}
