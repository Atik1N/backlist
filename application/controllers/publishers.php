<?php

class ControllerPublishers extends Controller
{
    private $error = array();

    function __construct()
    {
        $this->model_publishers = new ModelPublishers();
        $this->view = new View();
    }

	function index()
	{
        $data['text_title'] = 'Список издателей';
        $data['publishers'] = $this->model_publishers->show();
        $data['errors'] = $this->error;
		$this->view->load('publishers.php', 'template.php', $data);
	}

    function view($publisher_id)
    {
        $data['publisher'] = $this->model_publishers->show($publisher_id);
        $data['crud'] = 'view';
        $data['text_title'] = 'Просмотр информации об издателе';
        if($data['publisher']) {
            $data['form_link'] = HTTP_HOST . "publishers/" . $data['crud'] . "/" . $publisher_id;
            $data['publisher'] = $this->model_publishers->row($data['publisher']);
            $this->view->load('publisher_form.php', 'template.php', $data);
        } else{
            $this->view->__404();
        }
    }

    function edit($route_publisher_id)
    {
        if (($_SERVER['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_publishers->update($_POST);
            $this->redirect('publishers');
        }
        if(isset($_POST['publisher_id'])){

            $publisher_id = $_POST['publisher_id'];
        } else if ($route_publisher_id) {

            $publisher_id = $route_publisher_id;
        } else {
            $publisher_id = 0;
        }
        $data['publisher'] = $this->model_publishers->show($publisher_id);
        $data['text_title'] = 'Редактирование информации об издателе';
        $data['crud'] = 'edit';
        if($data['publisher']) {
            $data['form_link'] = HTTP_HOST . "publishers/" . $data['crud'] . "/" . $publisher_id;
            $data['publisher'] = $this->model_publishers->row($data['publisher']);
            $this->view->load('publisher_form.php', 'template.php', $data);
        } else{
            $this->view->__404();
        }
    }

    function create()
    {
        if (($_SERVER['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_publishers->insert($_POST);
            $this->redirect('publishers');
        }
        $data['crud'] = 'create';
        $data['form_link'] = HTTP_HOST . "publishers/" . $data['crud'];
        $data['text_title'] = 'Добавить издателя';
        $data['publisher'] = $_POST;
        $data['errors'] = $this->error;
        $this->view->load('publisher_form.php', 'template.php', $data);
    }

    function delete($publisher_id)
    {
        if((int)$publisher_id !== 0) {
            if($this->validateDelete($publisher_id)) {
                $this->model_publishers->delete($publisher_id);
                $this->redirect('publishers');
            } else {
                $this->index();
            }
        } else {
            $this->view->__404();
        }
    }

    function validate()
    {
        if ((mb_strlen($_POST['name']) < 3) || (mb_strlen($_POST['name']) > 255)) {
            $this->error['name'] = 'Введите имя';
        }

        if ((mb_strlen($_POST['address']) < 3) || (mb_strlen($_POST['address']) > 255)) {
            $this->error['address'] = 'Введите адрес';
        }

        if ((mb_strlen($_POST['telephone']) < 3) || (mb_strlen($_POST['telephone']) > 255)) {
            $this->error['telephone'] = 'Введите телефон';
        }
        return !$this->error;
    }

    private function validateDelete($publisher_id)
    {
        $result = $this->model_publishers->check($publisher_id);
        if((int)$result){
            $this->error['system_notification'][] = "Удаление не возможно. Издательство указано в нескольких книгах: " . $result;
        }
        return !$this->error;
    }
}