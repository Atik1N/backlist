<?php

class ControllerBooks extends Controller
{
    private $error = array();

    function __construct()
    {
        $this->view = new View();
        $this->model_rubrics = new ModelRubrics();
        $this->model_authors = new ModelAuthors();
        $this->model_books = new ModelBooks();
        $this->model_publishers = new ModelPublishers();
    }

    function index()
    {
        $data['text_title'] = 'Список книг';
        $data['books'] = $this->model_books->show(false, true);
        $this->view->load('books.php', 'template.php', $data);
    }

    function view($book_id)
    {
        $data['book'] = $this->model_books->show($book_id, true);
        $data['text_title'] = 'Просмотр информации о книге';
        $data['crud'] = 'view';
        if($data['book']) {
            $data['form_link'] = HTTP_HOST . "books/" . $data['crud'] . "/" . $book_id;
            $data['books_list'] = $this->model_books->show();
            $data['book'] = $this->model_books->row($data['book']);
            $this->view->load('book.php', 'template.php', $data);
        } else{
            $this->view->__404();
        }

    }

    function edit($route_book_id)
    {
        if (($_SERVER['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_books->update($_POST);
            $this->redirect('books');
        }
        if(isset($_POST['book_id'])){

            $book_id = $_POST['book_id'];
        } else if ($route_book_id) {

            $book_id = $route_book_id;
        } else {
            $book_id = 0;
        }
        $data['crud'] = 'edit';
        $data['text_title'] = 'Добавить книгу';
        $data['text_images'] = 'Прикрепить изображения';

        $book = $this->model_books->show($book_id);
        $data['text_title'] = 'Редактирование информации о книге';
        $data['books_list'] = $this->model_books->show();
        if($book) {
            $data['form_link'] = HTTP_HOST . "books/" . $data['crud'] . "/" . $book_id;
            $data['book'] = $this->model_books->row($book);

            $data['rubrics_list'] = $this->model_rubrics->show();
            $data['authors_list'] = $this->model_authors->show();
            $data['publishers_list'] = $this->model_publishers->show();

            $this->view->load('book_form.php', 'template.php', $data);
        } else {
            $this->view->__404();
        }
    }

    function create()
    {
        if (($_SERVER['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_books->insert($_POST);
            $this->redirect('books');
        }
        $data['crud'] = 'create';
        $data['text_title'] = 'Добавить книгу';
        $data['text_images'] = 'Прикрепить изображения';

        $data['form_link'] = HTTP_HOST . "books/" . $data['crud'];
        $data['rubrics_list'] = $this->model_rubrics->show();
        $data['authors_list'] = $this->model_authors->show();
        $data['publishers_list'] = $this->model_publishers->show();

        $data['book'] = $_POST;
        $data['errors'] = $this->error;
        $this->view->load('book_form.php', 'template.php', $data);
    }

    function delete($book_id)
    {
        if((int)$book_id !== 0) {
            $this->model_books->delete($book_id);
            $this->redirect('books');
        } else {
            $this->view->__404();
        }
    }

    function validate()
    {
        if ((mb_strlen($_POST['name']) < 3) || (mb_strlen($_POST['name']) > 255)) {
            $this->error['name'] = 'Введите название';
        }
        if(!isset($_POST['authors_id']) || isset($_POST['authors_id']) && (count($_POST['authors_id']) == 0)) {
            $this->error['authors'] = 'Укажите автора';
        }
        if (mb_strlen($_POST['date_publication']) < 10) {
            $this->error['date_publication'] = 'Укажите дату публикации ';
        }
        if ((int)$_POST['publisher_id'] === 0) {
            $this->error['publisher'] = 'Укажите издательство';
        }
        if ((int)$_POST['rubric_id'] === 0) {
            $this->error['rubric'] = 'Укажите рубрику';
        }
        return !$this->error;
    }
}