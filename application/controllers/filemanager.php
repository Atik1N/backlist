<?php

class ControllerFilemanager extends Controller
{
    private $error = array();
	
	function load()
	{
        $data = array();
        $data['images'] = array();
        if ($handle = opendir("images/books/")) {
            while (false !== ($file = readdir($handle))) {
                if(!in_array($file, array('.', '..'))) {
                    $data['images'][] = array(
                        'src'       => HTTP_HOST . "images/books/" . $file,
                        'path'      => "images/books/" . $file,
                        'name'      => $file
                    );
                }
            }
            closedir($handle);
        }
        $this->view->get('filemanager.php', $data);
	}

    function upload()
    {
        $response = array();
        foreach ($_FILES as $file){
            $filePath  = $file['tmp_name'];
            $oldName  = $file['name'];

            $fi = finfo_open(FILEINFO_MIME_TYPE);

            $mime = (string) finfo_file($fi, $filePath);

            if (strpos($mime, 'image') === false){
                $this->error[] = 'Файл ' . $oldName . ' не является изображением.';

            } else {
                $image = getimagesize($filePath);

                $name = md5_file($filePath);

                $extension = image_type_to_extension($image[2]);

                $format = str_replace('jpeg', 'jpg', $extension);
                if (!move_uploaded_file($filePath, realpath("images/books") . '/' . $name . $format)) {
                    $this->error[] = 'При записи изображения (' . $oldName . ') на диск произошла ошибка.';
                }
            }
        }
        if($this->error){
            $response['errors'] = $this->error;
        }
        echo json_encode($response);
    }
}
