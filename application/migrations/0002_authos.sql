-- Таблица versions --
CREATE TABLE if not exists `authors` (
  `author_id` int(11) not null AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
   primary key (author_id)
)
engine = innodb
auto_increment = 1
character set utf8
collate utf8_general_ci;