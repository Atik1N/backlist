CREATE TABLE if not exists  `publishers` (
  `publisher_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
   primary key (publisher_id)
)
engine = innodb
auto_increment = 1
character set utf8
collate utf8_general_ci;