CREATE TABLE if not exists  `books` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_publication` date NOT NULL,
   primary key (book_id)
)
engine = innodb
auto_increment = 1
character set utf8
collate utf8_general_ci;

CREATE TABLE if not exists  `book_images` (
  `book_images_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
   primary key (book_images_id)
)
engine = innodb
auto_increment = 1
character set utf8
collate utf8_general_ci;

CREATE TABLE if not exists  `book_to_author` (
  `book_to_author_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
   primary key (book_to_author_id)
)
engine = innodb
auto_increment = 1
character set utf8
collate utf8_general_ci;

CREATE TABLE if not exists  `book_to_publisher` (
  `book_to_publisher_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `publisher_id` int(11) NOT NULL,
   primary key (book_to_publisher_id)
)
engine = innodb
auto_increment = 1
character set utf8
collate utf8_general_ci;

CREATE TABLE if not exists  `book_to_rubric` (
  `book_to_rubric_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `rubric_id` int(11) NOT NULL,
   primary key (book_to_rubric_id)
)
engine = innodb
auto_increment = 1
character set utf8
collate utf8_general_ci;