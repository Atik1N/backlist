CREATE TABLE if not exists  `rubrics` (
  `rubric_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
   primary key (rubric_id)
)
engine = innodb
auto_increment = 1
character set utf8
collate utf8_general_ci;