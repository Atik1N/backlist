
--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`book_id`, `name`, `date_publication`) VALUES
(1, 'Преступление и наказание', '1973-02-21'),
(2, 'Анна Каренина', '2002-07-02'),
(3, 'Маленький принц', '1991-05-13'),
(4, 'Три товарища', '2018-06-23');


--
-- Дамп данных таблицы `book_images`
--

INSERT INTO `book_images` (`book_images_id`, `book_id`, `path`) VALUES
(1, 1, 'images/books/placehold--1.png'),
(2, 2, 'images/books/placehold--2.png'),
(3, 2, 'images/books/placehold--3.png'),
(4, 3, 'images/books/placehold--1.png'),
(5, 3, 'images/books/placehold--3.png'),
(6, 4, 'images/books/a6c52f11b8342ed107e408c5532e042a.jpg'),
(7, 4, 'images/books/placehold--1.png'),
(8, 4, 'images/books/placehold--2.png');


--
-- Дамп данных таблицы `book_to_author`
--

INSERT INTO `book_to_author` (`book_to_author_id`, `book_id`, `author_id`) VALUES
(1, 1, 4),
(2, 2, 3),
(3, 3, 2),
(4, 4, 1);


--
-- Дамп данных таблицы `book_to_publisher`
--

INSERT INTO `book_to_publisher` (`book_to_publisher_id`, `book_id`, `publisher_id`) VALUES
(1, 1, 3),
(2, 2, 2),
(3, 3, 1),
(4, 4, 2);


--
-- Дамп данных таблицы `book_to_rubric`
--

INSERT INTO `book_to_rubric` (`book_to_rubric_id`, `book_id`, `rubric_id`) VALUES
(1, 1, 24),
(2, 2, 12),
(3, 3, 23),
(4, 4, 25);