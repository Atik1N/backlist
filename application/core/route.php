<?php

class Route
{

	static function start()
	{
		$controller_name = 'migration';
		$action_name = 'index';
        $action_data = '';
		$routes = explode('/', $_SERVER['REQUEST_URI']);

		if ( !empty($routes[1]) )
		{	
			$controller_name = $routes[1];
		}
		if ( !empty($routes[2]) )
		{
			$action_name = $routes[2];
		}

        if ( !empty($routes[3]) )
        {
            $action_data = $routes[3];
        }

        if ($handle = opendir("application/models/")) {
            while (false !== ($model_file = readdir($handle))) {
                if(!in_array($model_file, array('.', '..'))) {
                    include "application/models/" . $model_file;
                }
            }
        }
        closedir($handle);

		$controller_file = strtolower($controller_name).'.php';
		$controller_path = "application/controllers/".$controller_file;
		if(file_exists($controller_path))
		{
			include "application/controllers/".$controller_file;
		}
		else
		{
			Route::ErrorPage404();
		}
        $controller_name = 'Controller'.ucfirst ($controller_name);

		$controller = new $controller_name;
		$action = $action_name;
		if(method_exists($controller, $action))
		{
			$controller->$action($action_data);
		}
		else
		{
			Route::ErrorPage404();
		}
	
	}

	function ErrorPage404()
	{
        $host = 'https://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.'404');
    }
    
}
