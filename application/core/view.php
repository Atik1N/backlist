<?php

class View
{

	function load($content_view, $template_view, $data = null)
	{
		if(is_array($data)) {

			extract($data);
		}

		include 'application/views/'.$template_view;
	}

    function get($content_view, $data = null)
    {
        if(is_array($data)) {

            extract($data);
        }

        include 'application/views/'.$content_view;
    }

    function __404()
    {
        $data['text_title'] = 'Страница не найдена';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        $this->load('404.php', 'template.php', $data);
    }
}
