<?php

class Model
{
    protected $connect ;

    function  __construct  (){
        $this->connect = new mysqli(DB_HOST,DB_NAME,DB_PASS,DB_USER) or die(mysqli_error($this->connect));
        $this->connect->set_charset("utf8");
    }

    function __destruct() {

        $this->connect->close();
    }

	public function show() {}

    public function insert($data){}

    public function update($data) {}

    public function delete($publisher_id) {}

    public function escape($string)
    {
        return addslashes($string);
    }

    public function row($result)
    {
        return $result[0];
    }

    public function query($sql)
    {
        return $this->connect->query($sql);
    }
}