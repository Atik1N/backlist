
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $text_title; ?></h3>
                <div class="btn-container">
                    <a href="<?php echo HTTP_HOST . "books/create/"; ?>" class="btn btn-default"><i class="fa fa-plus"></i> Добавить</a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Название</th>
                        <th scope="col">Рубрика</th>
                        <th scope="col">Управление</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(isset($books) && count($books) > 0) { ?>
                        <?php foreach($books as $k=>$book) { ?>
                            <tr>
                                <th scope="row"><?php echo $k+1; ?></th>
                                <td><?php echo $book['name']; ?></td>
                                <td>
                                    <?php foreach ($book['rubric_id'] as $rubric) { ?>
                                        <p><?php echo $rubric['name']; ?></p>
                                    <?php } ?>
                                </td>
                                <td>
                                    <a href="<?php echo HTTP_HOST . "books/view/" .$book['book_id']; ?>" class="btn btn-info"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a href="<?php echo HTTP_HOST . "books/edit/" .$book['book_id']; ?>" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="<?php echo HTTP_HOST . "books/delete/" .$book['book_id']; ?>" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <th colspan="5"><center><h2>Данные отсутствуют</h2></center></th>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>

