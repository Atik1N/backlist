
<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $text_title; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo $form_link; ?>" method="post" enctype="multipart/form-data" id="form-rubric" class="form-horizontal">
                <div class="box-body">
                    <div class="form-group">
                        <label for="parent_id" class="col-sm-2 control-label">Родительская категория</label>
                        <div class="col-sm-10">
                            <select id="parent_id" name="parent_id"  class="form-control" <?php if($crud == 'view') echo 'disabled'; ?>>
                                <option value="0">- Не выбрано -</option>
                                <?php if(isset($rubrics_list)) { ?>
                                    <?php foreach ($rubrics_list as $item) { if(isset($rubric['rubric_id']) && $rubric['rubric_id'] == $item['rubric_id']) continue; ?>
                                        <option value="<?php echo $item['rubric_id']; ?>" <?php if(isset($rubric['parent_id']) && $rubric['parent_id'] == $item['rubric_id']) echo 'selected'; ?>><?php echo $item['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group <?php if(isset($errors['name'])) echo 'has-error'; ?>">
                        <label for="name" class="col-sm-2 control-label">Название</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Введите название" value="<?php if(isset($rubric['name'])) echo $rubric['name']; ?>" <?php if($crud == 'view') echo 'disabled'; ?>>
                            <input type="hidden" id="rubric_id" name="rubric_id" value="<?php if(isset($rubric['rubric_id'])) echo $rubric['rubric_id']; ?>">
                            <span class="help-block"><?php if(isset($errors['name'])) echo $errors['name']; ?></span>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="<?php echo HTTP_HOST . "rubrics/"; ?>" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i></i> Назад</a>
                    <?php if($crud != 'view') { ?>
                        <button type="submit" form="form-rubric" class="btn btn-default pull-right">Сохранить</button>
                    <?php } ?>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>



