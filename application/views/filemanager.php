<div class="row">
    <input type="file" id="fm-input-load" name="fm-images" accept="image/*" style="display: none;">
    <div class="col-md-2">
        <div class="box box-solid">
            <div class="box-body fm-load-img">
                <img class="fm-image" src="<?php echo HTTP_HOST . 'images/plus.png'; ?>" alt="plus" style="max-width: 100%">
                <p style="text-align: center; margin-top: 5px;">Загрузить изображение</p>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <?php foreach ($images as $image) { ?>
    <div class="col-md-2">
        <div class="box box-solid">
            <div class="box-body fm-container-img">
                <img class="fm-image" src="<?php echo $image['src']; ?>" alt="<?php echo $image['name']; ?>" style="max-width: 100%">
                <p style="text-align: center; margin-top: 5px;"><?php echo $image['name']; ?></p>
                <input class="img-path" type="hidden" value="<?php echo $image['path'] ; ?>" />
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
    <?php } ?>
</div>