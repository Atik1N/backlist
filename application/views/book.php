
<div class="row">
    <div class="col-md-8">
        <div class="box box-solid">
            <div class="box-body">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                    <div class="carousel-inner">
                        <?php $i = true; foreach ($book['images'] as $src) { ?>
                            <div class="item <?php if($i) { $i = false; echo 'active'; } ?>">
                                <img src="<?php echo HTTP_HOST . $src; ?>" alt="#">
                            </div>
                        <?php } ?>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="fa fa-angle-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="fa fa-angle-right"></span>
                    </a>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <div class="col-md-4">
        <div class="box box-solid">
            <div class="box-body">
                <h2><?php echo $book['name']; ?></h2>
                <table class="table table-striped table-book-info">
                    <tbody>
                    <tr>
                        <td> Рубрика </td>
                        <td>
                            <?php foreach ($book['rubric_id'] as $rubric) { ?>
                                <p><?php echo $rubric['name']; ?></p>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td> Дата публикации </td>
                        <td> <?php echo $book['date_publication']; ?> </td>
                    </tr>
                    <tr>
                        <td> Автор(-ы) </td>
                        <td>
                            <?php foreach ($book['authors_id'] as $author) { ?>
                                <p><?php echo $author['name']; ?></p>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td> Издательство </td>
                        <td>
                            <?php foreach ($book['publisher_id'] as $publisher) { ?>
                                <p><?php echo $publisher['name']; ?></p>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td> Адрес издательства </td>
                        <td>
                            <?php foreach ($book['publisher_id'] as $publisher) { ?>
                                <p><?php echo $publisher['address']; ?></p>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td> Телефон издательства </td>
                        <td>
                            <?php foreach ($book['publisher_id'] as $publisher) { ?>
                                <p><?php echo $publisher['telephone']; ?></p>
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <p></p>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

