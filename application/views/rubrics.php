
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $text_title; ?></h3>
                <div class="btn-container">
                    <a href="<?php echo HTTP_HOST . "rubrics/create/"; ?>" class="btn btn-default"><i class="fa fa-plus"></i> Добавить</a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Название</th>
                        <th scope="col">Управление</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(isset($rubrics) && count($rubrics) > 0) { ?>
                        <?php foreach($rubrics as $rubric) {
                            $tab = '';
                            if( $rubric['level'] != 0){
                                for ($i = 0; $i < $rubric['level']; $i ++) {
                                    $tab .= '<i class="fa fa-ellipsis-v" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;';
                                }
                            }
                            $rubric['name'] = $tab . $rubric['name'];
                            ?>
                            <tr>
                                <td><?php echo $rubric['name']; ?></td>
                                <td>
                                    <a href="<?php echo HTTP_HOST . "rubrics/view/" .$rubric['rubric_id']; ?>" class="btn btn-info"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a href="<?php echo HTTP_HOST . "rubrics/edit/" .$rubric['rubric_id']; ?>" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="<?php echo HTTP_HOST . "rubrics/delete/" .$rubric['rubric_id']; ?>" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <th colspan="5"><center><h2>Данные отсутствуют</h2></center></th>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>

