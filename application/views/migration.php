
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $text_title; ?></h3>
                <div class="btn-container">
                    <?php if($relevance_status){ ?>
                        <div class="btn btn-default" disabled ><i class="fa fa-plus"></i> Запустить</div>
                    <?php } else { ?>
                        <a href="<?php echo HTTP_HOST . "migration/start/"; ?>" class="btn btn-default"><i class="fa fa-plus"></i> Запустить</a>
                    <?php }  ?>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Название</th>
                        <th scope="col" style="width: 130px;">Статус</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(isset($migrations) && count($migrations) > 0) { ?>
                        <?php foreach ($migrations as $k=>$migration) { ?>
                        <tr>
                            <th scope="row"><?php echo $k+1; ?></th>
                            <td><?php echo $migration['name']; ?></td>
                            <?php if($migration['status']) { ?>
                            <td><p class="text-green">Загружена</p></td>
                            <?php } else { ?>
                            <td><p class="text-red">Новая</p></td>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <th colspan="5"><center><h2>Данные отсутствуют</h2></center></th>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>

