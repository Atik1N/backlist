
<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $text_title; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo $form_link; ?>" method="post" enctype="multipart/form-data" id="form-publisher" class="form-horizontal">
                <div class="box-body">
                    <div class="form-group <?php if(isset($errors['name'])) echo 'has-error'; ?>">
                        <label for="name" class="col-sm-2 control-label">Название</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Введите название" value="<?php if(isset($publisher['name'])) echo $publisher['name']; ?>" <?php if($crud == 'view') echo 'disabled'; ?>>
                            <span  class="help-block"><?php if(isset($errors['name'])) echo $errors['name']; ?></span>
                        </div>
                    </div>
                    <div class="form-group <?php if(isset($errors['address'])) echo 'has-error'; ?>">
                        <label for="address" class="col-sm-2 control-label">Адрес</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="address" name="address" placeholder="Введите адрес" value="<?php if(isset($publisher['address'])) echo $publisher['address']; ?>" <?php if($crud == 'view') echo 'disabled'; ?>>
                            <span class="help-block"><?php if(isset($errors['address'])) echo $errors['address']; ?></span>
                        </div>
                    </div>
                    <div class="form-group <?php if(isset($errors['telephone'])) echo 'has-error'; ?>">
                        <label for="telephone" class="col-sm-2 control-label">Телефон</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Введите телефон" value="<?php if(isset($publisher['telephone'])) echo $publisher['telephone']; ?>" <?php if($crud == 'view') echo 'disabled'; ?>>
                            <input type="hidden" id="publisher_id" name="publisher_id" value="<?php if(isset($publisher['publisher_id'])) echo $publisher['publisher_id']; ?>">
                            <span class="help-block"><?php if(isset($errors['telephone'])) echo $errors['telephone']; ?></span>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="<?php echo HTTP_HOST . "publishers/"; ?>" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i></i> Назад</a>
                    <?php if($crud != 'view') { ?>
                        <button type="submit" form="form-publisher" class="btn btn-default pull-right">Сохранить</button>
                    <?php } ?>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>



