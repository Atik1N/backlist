
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $text_title; ?></h3>
                <div class="btn-container">
                    <a href="<?php echo HTTP_HOST . "publishers/create/"; ?>" class="btn btn-default"><i class="fa fa-plus"></i> Добавить</a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Название</th>
                        <th scope="col">Адрес</th>
                        <th scope="col">Телефон</th>
                        <th scope="col">Управление</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(isset($publishers) && count($publishers) > 0) { ?>
                        <?php foreach($publishers as $k=>$publisher) { ?>
                            <tr>
                                <th scope="row"><?php echo $k+1; ?></th>
                                <td><?php echo $publisher['name']; ?></td>
                                <td><?php echo $publisher['address']; ?></td>
                                <td><?php echo $publisher['telephone']; ?></td>
                                <td>
                                    <a href="<?php echo HTTP_HOST . "publishers/view/" .$publisher['publisher_id']; ?>" class="btn btn-info"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a href="<?php echo HTTP_HOST . "publishers/edit/" .$publisher['publisher_id']; ?>" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="<?php echo HTTP_HOST . "publishers/delete/" .$publisher['publisher_id']; ?>" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <th colspan="5"><center><h2>Данные отсутствуют</h2></center></th>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>

