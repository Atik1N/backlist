
<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $text_title; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo $form_link; ?>" method="post" enctype="multipart/form-data" id="form-author" class="form-horizontal">
                <div class="box-body">
                    <div class="form-group <?php if(isset($errors['name'])) echo 'has-error'; ?>">
                        <label for="name" class="col-sm-2 control-label">Имя</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Введите имя" value="<?php if(isset($author['name'])) echo $author['name']; ?>" <?php if($crud == 'view') echo 'disabled'; ?>>
                            <input type="hidden" id="author_id" name="author_id" value="<?php if(isset($author['author_id'])) echo $author['author_id']; ?>">
                            <span class="help-block"><?php if(isset($errors['name'])) echo $errors['name']; ?></span>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="<?php echo HTTP_HOST . "authors/"; ?>" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i></i> Назад</a>
                    <?php if($crud != 'view') { ?>
                        <button type="submit" form="form-author" class="btn btn-default pull-right">Сохранить</button>
                    <?php } ?>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>



