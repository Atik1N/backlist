
<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $text_title; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo $form_link; ?>" method="post" enctype="multipart/form-data" id="form-book" class="form-horizontal">
                <div class="box-body">
                    <div class="form-group <?php if(isset($errors['name'])) echo 'has-error'; ?>">
                        <label for="name" class="col-sm-2 control-label">Название</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Введите название" value="<?php if(isset($book['name'])) echo $book['name']; ?>" <?php if($crud == 'view') echo 'disabled'; ?>>
                            <input type="hidden" id="book_id" name="book_id" value="<?php if(isset($book['book_id'])) echo $book['book_id']; ?>">
                            <span class="help-block"><?php if(isset($errors['name'])) echo $errors['name']; ?></span>
                        </div>
                    </div>
                    <div class="form-group <?php if(isset($errors['authors'])) echo 'has-error'; ?>">
                        <label for="authors_id" class="col-sm-2 control-label">Авторы</label>
                        <div class="col-sm-10">
                            <select name="authors_id[]" class="form-control select-authors" <?php if($crud == 'view') echo 'disabled'; ?>  multiple="multiple" data-placeholder="Выберите авторов">
                                <?php if(isset($authors_list)) { ?>
                                    <?php foreach ($authors_list as $item) { ?>
                                        <option value="<?php echo $item['author_id']; ?>" <?php if(isset($book['authors_id']) && in_array($item['author_id'], $book['authors_id'])) echo 'selected'; ?>><?php echo $item['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <span class="help-block"><?php if(isset($errors['authors'])) echo $errors['authors']; ?></span>
                        </div>
                    </div>
                    <div class="form-group <?php if(isset($errors['publisher'])) echo 'has-error'; ?>">
                        <label for="publisher_id" class="col-sm-2 control-label">Издательство</label>
                        <div class="col-sm-10">
                            <select name="publisher_id" class="form-control" <?php if($crud == 'view') echo 'disabled'; ?> data-placeholder="Выберите издательство">
                                <option value="0">- Не выбрано -</option>
                                <?php if(isset($publishers_list)) { ?>
                                    <?php foreach ($publishers_list as $item) { ?>
                                        <option value="<?php echo $item['publisher_id']; ?>" <?php if(isset($book['publisher_id']) && $book['publisher_id'] == $item['publisher_id']) echo 'selected'; ?>><?php echo $item['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <span class="help-block"><?php if(isset($errors['publisher'])) echo $errors['publisher']; ?></span>
                        </div>
                    </div>
                    <div class="form-group <?php if(isset($errors['rubric'])) echo 'has-error'; ?>">
                        <label for="rubric_id" class="col-sm-2 control-label">Рубрика</label>
                        <div class="col-sm-10">
                            <select id="rubric" name="rubric_id"  class="form-control" <?php if($crud == 'view') echo 'disabled'; ?>>
                                <option value="0">- Не выбрано -</option>
                                <?php if(isset($rubrics_list)) { ?>
                                    <?php foreach ($rubrics_list as $item) { ?>
                                        <option value="<?php echo $item['rubric_id']; ?>" <?php if(isset($book['rubric_id']) && $book['rubric_id'] == $item['rubric_id']) echo 'selected'; ?>><?php echo $item['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <span class="help-block"><?php if(isset($errors['rubric'])) echo $errors['rubric']; ?></span>
                        </div>
                    </div>
                    <div class="form-group <?php if(isset($errors['date_publication'])) echo 'has-error'; ?>">
                        <label for="date_publication" class="col-sm-2 control-label">Дата публикации</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="date_publication" name="date_publication" placeholder="Выберите дату публикации" value="<?php if(isset($book['date_publication'])) echo $book['date_publication']; ?>" <?php if($crud == 'view') echo 'disabled'; ?>>
                            <span class="help-block"><?php if(isset($errors['date_publication'])) echo $errors['date_publication']; ?></span>
                        </div>
                    </div>
                    <div class="box-header with-border" style="margin-bottom: 10px;">
                        <h3 class="box-title"><?php echo $text_images; ?></h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-1" id="container-new-image" >
                            <a class="btn btn-app" data-toggle="modal" data-target="#modal-image-manager">
                                <i class="fa fa-edit"></i> Прикрепить
                            </a>
                        </div>
                        <?php if(isset($book['images'])) { ?>
                            <?php foreach ($book['images'] as $item) { ?>
                                <div class="col-sm-1" style="text-align: center;">
                                    <img class="book-img-preview" title="Нажмите для предпросмотра" src="<?php echo HTTP_HOST . $item; ?>" alt="preview" />
                                    <input type="hidden" value="<?php echo $item; ?>" name="images[]"/>
                                    <span onclick="$(this).parent().remove()" style="cursor:pointer;">Удалить</span>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="<?php echo HTTP_HOST . "books/"; ?>" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i></i> Назад</a>
                    <?php if($crud != 'view') { ?>
                        <button type="submit" form="form-book" class="btn btn-default pull-right">Сохранить</button>
                    <?php } ?>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>
<div class="modal fade" id="modal-image-manager" style="display: none;">
    <div class="modal-dialog" style="width: 90%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Менеджер изображений</h4>
            </div>
            <div class="modal-body" style="overflow: auto;height: 600px;">

                <div class="alert alert-warning alert-dismissible" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Внимание!</h4>
                    <div class="fm-notification-container"></div>
                </div>
                <div class="fm-content">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-primary" disabled id="add-new-image">Выбрать</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-image-preview" style="display: none;">
    <div class="modal-dialog" style="width: 90%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Предпросмотр изображения</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Page script -->
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select-authors').select2()

        //Date picker
        $('#date_publication').datepicker({
            format: 'dd.mm.yyyy',
            endDate: '+0d',
            defaultDate: new Date(),
            autoclose: true
        });

        $('#modal-image-manager').on('show.bs.modal', function () {

            $('.modal-body').children('.alert').css('display', 'none');
            refreshFM ();
        });

        $(document).on('click', '.book-img-preview', function () {
            var path = $(this).attr('src');
            $('#modal-image-preview').find('.modal-body').html("<img style='width: 100%;' src='" + path + "' alt='large-preview' />");
            $('#modal-image-preview').modal('show');
        });

        $(document).on('click', '#add-new-image', function () {
            if(select_image !== null) {
                var data = '<div class="col-sm-1" style="text-align: center;">\n' +
                    '                        <img class="book-img-preview" title="Нажмите для предпросмотра" src="<?php echo HTTP_HOST; ?>' + select_image + '" alt="preview" />\n' +
                    '                        <input type="hidden" value="' + select_image + '" name="images[]"/>\n' +
                    '                        <span onclick="$(this).parent().remove()" style="cursor:pointer;">Удалить</span>\n' +
                    '                    </div>';
                $('#container-new-image').after(data);
                select_image = null;
                $('#modal-image-manager').modal('hide');
            } else {

            }
        });

        var select_image = null;
        $(document).on('click', '.fm-container-img', function () {

            $('.fm-container-img').removeClass('fm-active');
            $(this).addClass('fm-active');
            select_image = $(this).children('.img-path').val();
            $('#add-new-image').prop("disabled", false);
        });

        $(document).on('dblclick ', '.fm-container-img', function () {

            $('.fm-container-img').removeClass('fm-active');
            $(this).addClass('fm-active');
            select_image = $(this).children('.img-path').val();
            $('#add-new-image').trigger('click');
        });

        $(document).on('click', '.fm-load-img', function () {
            $('.modal-body').children('.alert').css('display', 'none');
            $('#fm-input-load').trigger('click');
        });

        $(document).on('change', '#fm-input-load', function () {
            var files = this.files;
            // создадим объект данных формы
            var data = new FormData();

            // заполняем объект данных файлами в подходящем для отправки формате
            $.each( files, function( key, value ){
                data.append( key, value );
            });

            // добавим переменную для идентификации запроса
            data.append( 'fm-images', 1 );
            $.ajax({
                type: 'post',
                url: '<?php echo HTTP_HOST; ?>filemanager/upload',
                data: data,
                cache: false,
                processData : false,
                contentType : false,
                dataType : "json",
                success: function (data, textStatus) {
                    if(data['errors']){
                        var text_errors = '';
                        $.each(data['errors'], function (key, value) {
                            text_errors += "<p>" + value + "</p>";
                        });
                        $('.modal-body').children('.alert').css('display', 'block');
                        $('.fm-notification-container').html(text_errors);
                    } else {
                        refreshFM ();
                    }
                }
            });
        });
        function refreshFM () {
            $.ajax({
                url: '<?php echo HTTP_HOST; ?>filemanager/load',
                dataType : "html",
                success: function (data, textStatus) {
                    $('.fm-content').html(data);
                    $('#add-new-image').prop("disabled", true);
                }
            });
        }
    })
</script>



